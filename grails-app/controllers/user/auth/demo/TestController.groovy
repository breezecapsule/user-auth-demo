package user.auth.demo

import annotation.JSONWebTokenRequest
import grails.converters.JSON

class TestController {

    def authorizationService

    @JSONWebTokenRequest(['Admin', 'User'])
    def test() {
        println 'JWT Token from request:' + request.getHeader('Authentication')
        def subject = authorizationService.getCurrentSubject()
        render ([subject: subject.toMap()] as JSON)
    }
}
