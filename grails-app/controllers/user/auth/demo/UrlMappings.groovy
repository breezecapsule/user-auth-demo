package user.auth.demo

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "/test"(controller: 'test', action: 'test')
        "/test2"(controller: 'test', action: 'test2')
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
