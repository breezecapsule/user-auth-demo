<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    <style>
        body {
            padding: 20px 50px;
        }
        #token {
            padding: 10px;
        }
    </style>
</head>
<body>
<script type="application/javascript">

    function sendRequest(){
        $.ajax({
            url: '/test',
            headers: { 'Authentication': 'eyJhbGciOiJSUzUxMiJ9.eyJzY29wZXMiOlsicmVhbG06dXNlcnM6ZGVsZXRlIiwicmVhbG06dXNlcnM6cmVhZCIsInJlYWxtOnVzZXJzOmNyZWF0ZSIsInJlYWxtOnRva2VuczpyZXZva2UiLCJyZWFsbTpyZWFkIiwicmVhbG06dXNlcnM6dXBkYXRlIiwicmVhbG06dG9rZW5zOnJldm9rZTo5ODQ1Zjc4OC1kZTQ5LTRiMDctOTlhOC04YTljNWQyMzAxNmIiXSwiYXVkIjoiODNlYTI1ZDgtZGMyMS00Njg0LWFkZWUtY2Y2NjdkOWU0NmFmIiwic3ViIjoiNWE5MDMyNzYtODgxMC00YjdjLTg5Y2YtODdlODc2Mzc1M2Q3IiwiZXhwIjoxNDY4ODkyMzIzLCJpYXQiOjE0Njg4MDU5MjMsImp0aSI6Ijk4NDVmNzg4LWRlNDktNGIwNy05OWE4LThhOWM1ZDIzMDE2YiJ9.FMNkOTY8z3FYGYqwO-y_kb5fnQdwhuY7t0p7nEtG23fraDXDeD1yVopyIN0EEes8-Ux45ERxYQIKn5rukpNsTw7EMs5sjf_04iFMWczpkBYEGqQZvT1Ql7Ll-hqXK6PRy4kcKXSFd6FSgDK_ouSi57WQyjO7TC5JtPAXulb-ifsG-EaKsjkTybw-R4p-BAHPM8KWsnhFfASZEapv1rQCiY-l6JnrdbV4scOZmQOOzcnVE2i0cGCC49o4fHwQYniYAPwd3ef1olK7vHNBk_NBX1gNgoXHMKUA0nU3D0f9VUBPJ6AEIxETfffY4cR2a5FhLmEXWmpLfX8-z7zkyAJQLCBsesIvg2qMYzNr8GCcLKtbrVJlFN_erI1zlu1COAp_kTxHW3Vh4puBtykM1J8bzYzKcq8C8RJbwUQSjKhvViAepN9RVxjdmBcMrfACzlopUMoI2Fl_RccCWqqz-noqOpS8baimYg2miNAZdxQbot8PPf38KbywGvmwg3sTGoxSaxFtuMmceY3H7_rHrIvjs1CBV-5Lz_tlI1RJG-SzKlTFXRSLge8OBpticKAP0_rDMqFoEPILrhQzuSA_ytKz1lBYuHzxGAFwbRCu9O3XRpuUArWTXkmCgyLyad8C808JCrqumvBrbC-lcBuVbK2YeF95v3M7RGF2bml3LLTMTUU'},
            data: {
                realmId: '83ea25d8-dc21-4684-adee-cf667d9e46af'
            }
        }).done(function(response) {
            $("#result").html(JSON.stringify(response.subject))
        });
    }
</script>

<button onclick="sendRequest()">Send Request</button>
<pre id="result">

</pre>
</body>
</html>
